import { Schema, model } from "mongoose";
import type { IUserCompletedGame } from "~/types";

const UserCompletedGameSchema = new Schema<IUserCompletedGame>(
	{
		game: Number,
		user: Number,
		isOutdated: Boolean,
	},
	{
		timestamps: true,
	}
);

export const UserCompletedGame = model<IUserCompletedGame>("UserCompletedGames", UserCompletedGameSchema);
