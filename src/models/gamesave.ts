import { Schema, model } from "mongoose";
import type { IGameSave } from "~/types";

const GameSaveSchema = new Schema<IGameSave>(
	{
		game: Number,
		user: Number,
		slot: Number,
		label: {
			type: String,
			maxLength: 100,
		},
		summary: {
			type: String,
			maxLength: 2048,
		},
		data: {
			type: String,
			maxLength: 256000,
		},
		// would caching save length actually make it more efficient?
		length: Number,
	},
	{
		timestamps: true,
	}
);

export const GameSave = model<IGameSave>("GameSaves", GameSaveSchema);
