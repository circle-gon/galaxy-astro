import { Schema, model } from "mongoose";
import type { IForumCategory } from "~/types";

const ForumCategorySchema = new Schema<IForumCategory>({
	id: Number,
	slug: String,
	icon: {
		type: String,
		default: "ic:baseline-message",
	},
	color: {
		type: String,
		default: "default",
	},
	name: String,
	description: {
		type: String,
		default: "",
	},
	threadCount: {
		type: Number,
		default: 0,
	},
	postCount: {
		type: Number,
		default: 0,
	},
	lastThread: {
		type: Date,
		default: new Date(0),
	},
	lastPost: {
		type: Date,
		default: new Date(0),
	},
	parent: Schema.Types.Mixed,
	order: {
		type: Number,
		default: 0,
	},
	giveXP: {
		type: Boolean,
		default: false,
	},
	locked: {
		type: Boolean,
		default: false,
	},
	linkedGames: {
		type: [Number],
		default: [],
	},
});

export const ForumCategory = model<IForumCategory>(
	"ForumCategories",
	ForumCategorySchema
);
