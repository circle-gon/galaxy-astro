import type { AstroGlobal } from "astro";
import type { IUserAuth } from "~/types";

export const months =
	"January February March April May June July August September October November December".split(
		" "
	);

export function astroUnder13(Astro: AstroGlobal) {
	return Astro.locals.auth.isUnderage;
}

export function userUnder13(userAuth: IUserAuth): boolean {
	return monthUnder13(userAuth.birthYear ?? -1, userAuth.birthMonth ?? -1);
}

export function monthUnder13(year: number, month: number): boolean {	
	const currentYear = new Date().getFullYear();
	const currentMonth = new Date().getMonth();
	if (currentYear - year > 13) return false;
	else if (currentYear - year < 13) return true;

	// If in the year area where user could be 13 or 12
	if (currentMonth <= month) return true;
	else return false;
}
