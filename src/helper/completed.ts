import type { IUser, IUserCompletedGame } from "~/types";
import { UserCompletedGame } from "~/models/usercompletedgame";

export type UserCompletedGameMap = Map<IUserCompletedGame["game"], IUserCompletedGame>;

export function mapCompletedByGameId(completed: IUserCompletedGame[]): UserCompletedGameMap {
	const completedByGameId = new Map<number, IUserCompletedGame>();
	for (const game of completed) {
		completedByGameId.set(game.game, game);
	}
	return completedByGameId;
}

export async function getUserCompletedGames(id: IUser["id"]): Promise<UserCompletedGameMap> {
	const completes = await UserCompletedGame.find({ user: id }).lean()
	return mapCompletedByGameId(completes);
}

export function getCompletionIcon(completed: boolean, outdated: boolean): string {
	if(!completed) {
		return "material-symbols:check-box-outline-blank";
	}
	if(outdated) {
		return "mdi:alert-decagram";
	}

	return "material-symbols:check-box-outline-rounded";
}