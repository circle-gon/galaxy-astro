/**
 * Apply toFixed to a number and remove trailing "0" and ".".
 */
export function tidyToFixed(number: number, fractionDigits?: number): number {
	return Number(number.toFixed(fractionDigits));
}