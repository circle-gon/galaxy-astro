import { Game } from "~/models/game";
import { GameSave } from "~/models/gamesave";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
	slot: z.number().int().min(0, "Invalid slot").max(10, "Invalid slot"),
	data: z
		.string()
		.max(256_000, "Too big. The limit is 256,000 bytes.")
		.default(""),
	summary: z
		.string()
		.max(2048, "Summary is too long")
		.default(""),
	label: z.string().max(100, "Label too long.").default(""),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		let save = await GameSave.findOne({
			game: req.game,
			user: user.id,
			slot: req.slot,
		});

		const game = await Game.findOne({ id: req.game });
		if (!game) return rmsg("Game not found", 404);

		if (!save) {
			const count = await GameSave.countDocuments({ user: user.id });
			if (count >= 500)
				return rmsg(
					"You have reached the cap of 500 cloud saves. If you encounter this error, please inform site staff and the quota will probably be increased.",
					400
				);

			save = new GameSave({
				user: user.id,
				game: game.id,
				slot: req.slot,
				data: req.data,
				length: Buffer.byteLength(req.data),
				summary: req.summary,
				label: req.label,
			});
		} else {
			save.data = req.data;
			save.length = Buffer.byteLength(req.data);
			save.summary = req.summary;
			save.label = req.label;
		}

		await save.save();

		return rmsg("Save successful");
	}
);
