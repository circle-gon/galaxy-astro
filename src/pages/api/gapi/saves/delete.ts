import { GameSave } from "~/models/gamesave";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
	slot: z.number().int().min(0, "Invalid slot").max(10, "Invalid slot"),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		await GameSave.deleteOne({
			game: req.game,
			slot: req.slot,
			user: user.id,
		});

		return rmsg("Save deleted");
	}
);
