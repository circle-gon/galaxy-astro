import { GameSave } from "~/models/gamesave";
import { rdata } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
	slot: z.number().int().min(0, "Invalid slot").max(10, "Invalid slot"),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const save = await GameSave.findOne({
			game: req.game,
			user: user.id,
			slot: req.slot,
		});

		if (!save) return rdata({
			message: "Save does not exist",
			game: req.game,
			user: user.id,
			slot: req.slot,
		});

		return rdata({
			message: "Load successful",
			game: req.game,
			user: user.id,
			slot: req.slot,
			label: save.label,
			summary: save.summary,
			length: save.length || Buffer.byteLength(save.data),
			data: save.data,
			time: +save.updatedAt,
		});
	}
);
