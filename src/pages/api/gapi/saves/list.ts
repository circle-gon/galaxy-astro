import { GameSave } from "~/models/gamesave";
import { rdata } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const saves = await GameSave.find({
			game: req.game,
			user: user.id,
		});

		const frontendSaves = saves
			.sort((a, b) => a.slot - b.slot)
			.map(save => {
				let data = {
					game: req.game,
					user: user.id,
					slot: save.slot,
					label: save.label,
					summary: save.summary,
					length: save.length,
					data: undefined,
					time: +save.updatedAt,
				}
				if (save.length <= 0 || save.length === undefined) {
					data.length = save.length = Buffer.byteLength(save.data);
					save.save();
				}
				if (save.length < 8000) {
					data.data = save.data;
				}
				return data;
			});

		return rdata({
			message: "Load successful",
			game: req.game,
			user: user.id,
			saves: frontendSaves,
		});
	}
);
