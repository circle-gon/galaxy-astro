import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	user: z.number().int(),
});

export const POST = adminRoute(
	{
		schema,
	},
	async (_request, { user }, mod) => {
		const userAuth = await UserAuth.findOne({ id: user });
		if (!userAuth) return rmsg("User not found", 404);
		
		userAuth.parentState = 0;
		await userAuth.save();

		const userDoc = await User.findOne({ id: user })
		await logAction(
			mod.id,
			"user",
			`requsted birthday from ${userDoc.name} (${user})`,
			`(no elaboration)`,
			{ userId: user }
		);
	}
);
