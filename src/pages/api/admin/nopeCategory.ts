import { CategoryRequest } from "~/models/categoryrequest";
import { adminRoute } from "~/helper/route";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	slug: z.string(),
});

export const POST = adminRoute({ schema }, async (_request, { slug }) => {
	const catReq = await CategoryRequest.findOne({ slug, resolved: false });
	if (!catReq) return rmsg("Category request not found", 400);

	catReq.resolved = true;

	await catReq.save();
});
