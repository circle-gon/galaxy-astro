import { User } from "~/models/user";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	page: z.number().int().min(1).default(1)
});

export const POST = adminDataRoute(
	{
		reqLevel: "verifier",
		schema,
	},
	async (_request, data) => {
		const pageCount = Math.ceil((await User.estimatedDocumentCount()) / 100);
		const result = (await User.find({}).sort("-id").limit(100).skip(100 * (data.page - 1))).map(
			x => x.toObject()
		);

		return { result, pageCount };
	}
);
