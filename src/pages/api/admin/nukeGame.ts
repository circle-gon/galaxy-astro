import { Comment } from "~/models/comment";
import { Favorite } from "~/models/favorite";
import { Game } from "~/models/game";
import { Rating } from "~/models/rating";
import { Update } from "~/models/update";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { env } from "~/helper/env";
import { logAction } from "~/helper/adminHelper";
import { rmsg } from "~/helper/res";
import sendWebhook from "~/helper/sendWebhook";
import sharp from "sharp";
import { updateThumbs } from "~/helper/img";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = adminRoute(
	{ reqLevel: "verifier", schema },
	async (_request, { game }, mod) => {
		const g = await Game.findOne({ id: game });
		if (!g) return rmsg("Game not found", 404);

		const author = await User.findOne({ id: g.author });
		await logAction(
			mod.id,
			"game",
			`deleted game ${g.name} (${g.id})`,
			`posted by ${author.name} (${author.id}). verification status: ${g.verified}`,
			{ userId: g.author, game: g }
		);

		if (g.verified !== true) {
			const unv = await Game.countDocuments({ verified: { $ne: true } });
			await sendWebhook(env.STAFF_GAME_WEBHOOK, {
				content: `unverified game ${g.name} has been deleted. ${unv} left.`,
			});
		}

		const original = g.thumbTimestamp;
		g.thumbTimestamp = Date.now();
		await g.save();

		await Update.deleteMany({ game });
		await Rating.deleteMany({ game });
		await Comment.deleteMany({ game });
		await Favorite.deleteMany({ game });
		await updateThumbs(
			sharp("public/unknown-game.webp"),
			game,
			g.thumbTimestamp,
			original
		);

		await Game.deleteOne({ id: game });
	}
);
