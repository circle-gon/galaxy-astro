import { User } from "~/models/user";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	users: z.array(z.number()),
});

export const POST = adminDataRoute({ reqLevel: "any", schema }, async (_request, { users }) => {
	const result = [...(await User.find({ id: { $in: users } })).values()].map(
		user => user.toObject()
	);
	return { result };
});
