import { CategoryModerator } from "~/models/categorymod";
import { CategoryRequest } from "~/models/categoryrequest";
import { ForumCategory } from "~/models/forumcategory";
import { adminRoute } from "~/helper/route";
import { keyv } from "~/models/keyv";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	slug: z.string(),
	parent: z.string(),
});

export const POST = adminRoute(
	{ schema },
	async (_request, { slug, parent: parentSlug }) => {
		const catReq = await CategoryRequest.findOne({ slug, resolved: false });
		if (!catReq) return rmsg("Category request not found", 400);

		const parent = await ForumCategory.findOne({ slug: parentSlug });
		if (!parent) return rmsg("Parent category not found", 400);

		const id = await keyv.id("lastCategoryId");
		const cat = new ForumCategory({
			id,
			slug,
			name: catReq.name,
			description: catReq.description,
			parent: parent.id,
		});

		const permissionNode = new CategoryModerator({
			perms: 3,
			user: catReq.requestAuthor,
			category: id,
		});

		catReq.resolved = true;

		await Promise.all([cat.save(), permissionNode.save(), catReq.save()]);
	}
);
