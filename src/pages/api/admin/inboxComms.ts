import { Message } from "~/models/message";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";
import { z } from "zod";

const schema = z.object({
	title: z.string(),
	content: z.string(),
	to: z.literal(true).or(z.number().int()),
});

export const POST = adminRoute(
	{
		schema,
		reqLevel: "verifier",
	},
	async (_request, { title, content, to }, mod) => {
		if (to === true) {
			// I will re-enable this in a commit if it is needed. For the time being,
			// it just seems needlessly risky.
			// const users = await User.find({
			// 	banned: false,
			// 	emailVerified: true,
			// });
			// const reqs = users.map(user =>
			// 	new Message({
			// 		content,
			// 		title,
			// 		from: -1,
			// 		to: user.id,
			// 	}).save()
			// );
			// await Promise.all(reqs);
		} else {
			const msg = new Message({
				content,
				title,
				from: -1,
				to,
			});
			await msg.save();

			const user = await User.findOne({ id: to });

			await logAction(
				mod.id,
				"comms",
				`sent a message to ${user.name} (${user.id})`,
				`title: ${title}

content:
${content}`,
				{ userId: to, msg }
			);
		}
	}
);
