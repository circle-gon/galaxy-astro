import { ForumCategory } from "~/models/forumcategory";
import { forumModeratorLevel } from "~/helper/forum";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	locked: z.boolean(),
	icon: z
		.string()
		.nonempty("Please specify an icon")
		.max(100, "Icon ID is too long"),
	color: z.enum([
		"default",
		"red",
		"orange",
		"yellow",
		"green",
		"teal",
		"blue",
		"purple",
	]),
	name: z
		.string()
		.nonempty("Please specify a name")
		.max(128, "Category name too long"),
	description: z.string().max(512, "Description too long"),
	category: z.number().int(),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { locked, icon, color, name, description, category: catId } = req;

		const category = await ForumCategory.findOne({ id: catId });
		if (!category) return rmsg("Category not found", 400);

		const mod = await forumModeratorLevel(user, category);
		if (mod === 0) return rmsg("Insufficient permissions", 400);

		await ForumCategory.updateOne(
			{ id: catId },
			{ $set: { locked, icon, color, name, description } }
		);

		return rmsg("ok");
	}
);
