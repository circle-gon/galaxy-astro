import { rfu, rmsg } from "~/helper/res";
import { Comment } from "~/models/comment";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	content: z
		.string()
		.min(1, "Reply must not be empty")
		.max(8192, "Reply is too long"),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		schema,
	},
	async ({ locals }, { id: commentId, content: reply }) => {
		const { user } = locals.auth;

		const comment = await Comment.findOne({
			id: commentId,
			deleted: false,
		});
		if (!comment)
			return rmsg("Attempted to reply to an unknown comment", 404);

		const game = await Game.findOne({ id: comment.game });
		if (!game) return rmsg("Attempted to reply to an unknown game", 404);
		if (game.author !== user.id)
			return rfu("You didn't make this game, silly!", 403);

		comment.devResponse = reply;
		await comment.save();

		// Don't notify the dev if they reply to their own comment.
		if (game.author !== comment.author) {
			const msg = new Message({
				from: -1,
				to: comment.author,
				title: `A developer replied to your comment on ${game.name}`,
				content: `${user.name} replied to your comment!

Your comment:
> ${comment.content}

${user.name}'${user.name.endsWith("s") ? "" : "s"} reply:
> ${reply}`,
			});
			await msg.save();
		}

		return rmsg("success", 200);
	}
);
