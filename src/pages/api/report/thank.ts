import { Message } from "~/models/message";
import { Report } from "~/models/report";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";

export const POST = adminRoute({}, async (req, _data, mod) => {
	const { report } = await req.json();

	const reportDoc = await Report.findById(report);

	const message = new Message({
		from: -1,
		to: reportDoc.author,
		title: "Thank you!",
		content:
			"We've taken action based on a report you've made. Thank you for helping to keep Galaxy safe!",
	});

	await message.save();

	const reportAuthor = await User.findOne({ id: reportDoc.author });
	await logAction(
		mod.id,
		"report",
		`thanked ${reportAuthor.name} (${reportAuthor.id}) for a report`,
		`the report was about ${reportDoc.part} | ${reportDoc.reason}`,
		{
			userId: reportAuthor.id,
			report: reportDoc,
		}
	);
});
