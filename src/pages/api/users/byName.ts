import { rdata, rmsg } from "~/helper/res";
import { User } from "~/models/user";
import { route } from "~/helper/route";

export const GET = route({}, async ({ url }) => {
	const name = url.searchParams.get("name");

	const user = await User.findOne({ name });
	if (!user) return rmsg("no", 404);

	return rdata({
		message: "yes",
		name: user.name,
		equippedFlair: user.equippedFlair,
		pfpTimestamp: user.pfpTimestamp,
		id: user.id,
		banned: user.banned !== false,
	});
});
