import { rdata } from "~/helper/res";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals }) => {
		const { user } = locals.auth;

		return rdata(user);
	}
);
