import { UserAuth, fromEmail } from "~/models/userauth";
import { User } from "~/models/user";
import bcrypt from "bcryptjs";
import { generateTokenHeaders } from "~/models/userauth";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	email: z.string(),
	password: z.string({
		invalid_type_error: "Invalid password",
		required_error: "Password not provided",
	}),
});

export const POST = route(
	{
		schema,
	},
	async (_context, req) => {
		// Find an existing user
		let user = await fromEmail(req.email);
		if (!user) {
			user = await User.findOne({ name: req.email });
			if (!user) return rmsg("User not registered", 400);
			user = await UserAuth.findOne({ id: user.id });
		}

		if (req.password.length < 3 || req.password.length > 128)
			return rmsg("Wrong password", 400);

		const equal = await bcrypt.compare(req.password, user.password);
		if (!equal) return rmsg("Wrong password", 400);

		const pubUser = await User.findOne({ id: user.id });

		return new Response(
			JSON.stringify({
				name: pubUser.name,
				email: user.email,
				id: user.id,
			}),
			{
				status: 200,
				headers: generateTokenHeaders(user),
			}
		);
	}
);
