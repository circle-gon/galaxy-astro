// Temporary experimentation file.
// If this exists a month after 2024/09/08, it's probably a mistake.
// Feel free to open an MR to remove it!
// See also: UserManager.svelte

import { env } from "~/helper/env";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import { writeFile } from "fs/promises";
import { z } from "zod";

const schema = z.object({
	bio: z.string().max(1024),
	apples: z.number(),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		schema,
	},
	async ({ locals, request, clientAddress }, req) => {
		const { user } = locals.auth;
		const stamp = Date.now();

		const fancyRequest = {};
		for (const key in request) {
			if (key === "headers")
				fancyRequest.headers = Object.fromEntries(
					request.headers.entries()
				);
			else fancyRequest[key] = request[key];
		}

		const filePath = `${user.name}-${stamp}.txt`;

		// await writeFile(
		// 	filePath,
		// 	JSON.stringify({
		// 		...req,
		// 		clientAddress,
		// 		request: fancyRequest,
		// 	})
		// );

		sendWebhook(env.STAFF_REPORT_WEBHOOK, {
			content: `**bio trap tripped**`
				+ `\nuser: \`${user.name}\` (${user.id})`
				+ `\napples: ${+req.apples}`
				+ `\nbio: ${unsus(req.bio ?? "")}`,
		});

		return rmsg("Bio updated!");
	}
);

function unsus(message: string) {

	// Escape code block barrier
	message = message.replaceAll(/```/g, "");

	// Mark sus characters red
	// eslint-disable-next-line no-control-regex
	message = message.replaceAll(/([^\x00-\x7F]+)/g, "\x1B[41m$1\x1B[0m");

	console.log(message);

	message = "\n```ansi\n" + message + "\n```"
	return message;
}
