import { rdata, rmsg } from "~/helper/res";
import { User } from "~/models/user";
import { route } from "~/helper/route";

export const GET = route({}, async ({ url }) => {
	const id = parseInt(url.searchParams.get("id"));
	if (isNaN(id)) return rmsg("Invalid user ID", 400);

	const user = await User.findOne({ id });

	if (user) return rdata({ message: "yes", user });
	return rmsg("no", 404);
});
