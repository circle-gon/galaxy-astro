import { AF2024Participant } from "~/models/af2024participant";
import { keyv } from "~/models/keyv";
import { rdata } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

export const GET = route({}, async ({ locals }) => {
	if (locals.auth.loggedIn) {
		const [clicks, selfDoc] = await Promise.all([
			keyv.get("af2024", 0),
			AF2024Participant.findOne({ user: locals.auth.user.id }),
		]);
		return rdata({ clicks, self: selfDoc?.clicks ?? 0 });
	}
	const clicks = await keyv.get("af2024", 0);
	return rdata({ clicks, self: 0 });
});

export const POST = route(
	{
		auth: true,
		schema: z.number().int().min(0).max(25).or(z.literal("auto")),
	},
	async ({ locals }, _clickCount) => {
		const [clicks, selfDoc] = await Promise.all([
			keyv.get("af2024", 0),
			AF2024Participant.findOne({ user: locals.auth.user.id }),
		]);

		return rdata({ clicks, self: selfDoc?.clicks ?? 0 });
	}
);
