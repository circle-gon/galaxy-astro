import { rdata, rfu, rmsg } from "~/helper/res";
import { DayGameStats } from "~/models/daygamestats";
import { Game } from "~/models/game";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals, url }) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: url.searchParams.get("id") });
		if (!game) return rmsg("Game not found", 400);
		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);

		let timeframe = parseInt(url.searchParams.get("tf"));
		if (isNaN(timeframe) || !Number.isInteger(timeframe) || timeframe <= 0)
			timeframe = 9999;

		// I love MONGODB PIPELINES
		const ds = await DayGameStats.aggregate([
			{ $match: { game: game.id } },
			// {
			// 	$project: {
			// 		day: {
			// 			$floor: {
			// 				$divide: ["$startTime", 1000 * 60 * 60 * 24],
			// 			},
			// 		},
			// 		minutes: 1,
			// 	},
			// },
			// {
			// 	$group: {
			// 		_id: "$day",
			// 		// TODO also have a `day` prop for convenience/readability?
			// 		avg: { $avg: "$minutes" },
			// 		sum: { $sum: "$minutes" },
			// 	},
			// },
			{
				$project: {
					_id: 0,
					day: 1,
					minutes: 1,
					plays: 1,
				},
			},
			{ $sort: { day: -1 } },
			{ $limit: timeframe },
		]);

		return rdata({ message: "ok", ds });
	}
);
