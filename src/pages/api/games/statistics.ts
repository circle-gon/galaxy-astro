import { rdata, rfu, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { Playtime } from "~/models/playtime";
import { Rating } from "~/models/rating";
import { UserCompletedGame } from "~/models/usercompletedgame";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals, url }) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: url.searchParams.get("id") });
		if (!game) return rmsg("Game not found", 400);
		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);

		// TODO parallelize requests with promise.all
		const uniquePlayers = await Playtime.countDocuments({
			game: game.id,
		});

		const retainedPlayers = await Playtime.countDocuments({
			game: game.id,
			minutes: { $gte: 30 },
		});

		const ratingCounts = new Array(5).fill(0);
		const ratingQuery = await Rating.aggregate([
			{ $match: { game: game.id } },
			{
				$group: {
					_id: "$rating",
					count: { $count: {} },
				},
			},
		]);
		ratingQuery.forEach(res => {
			ratingCounts[res._id - 1] = res.count;
		});

		// Note : not sure that an $aggregate is more performant ? (must be tested with a lot of data to be sure...)
		//        An index on isOutdated is probably more efficient anyway
		const outdatedCompletionCount = await UserCompletedGame.countDocuments({
			game: game.id,
			isOutdated: true,
		});
		const completionCount = await UserCompletedGame.countDocuments({
			game: game.id,
			isOutdated: {
				$not: { $eq: true },
			},
		});

		// let retentionPercentiles = new Array(5).fill(0);
		/* 
		const retentionQuery = await Playtime.aggregate( [
			{ $match: { game: game.id } },
			{
			    $group: {
				  	_id: null,
				  	retentionPercentile: {
					 	$percentile: {
							input: "$minutes",
							p: [ 0.2, 0.4, 0.6, 0.8, 1 ],
							method: 'approximate'
					 	}
				  	},
			  	}
			}
		 ] )
		retentionQuery.forEach(res => {
			retentionPercentile = res.retentionPercentile;
		})
		*/

		return rdata({
			message: "ok",
			data: {
				ratingCounts,
				uniquePlayers,
				completion: {
					outdated: outdatedCompletionCount,
					upToDate: completionCount,
				},
				game,
				retention: { 30: retainedPlayers },
				// retentionPercentiles
			},
		});
	}
);
