import { rdata, rfu, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { Playtime } from "~/models/playtime";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals, url }) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: url.searchParams.get("id") });
		if (!game) return rmsg("Game not found", 400);
		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);

		const users = await Playtime.countDocuments({
			game: game.id,
			minutes: { $gte: parseInt(url.searchParams.get("mins")) },
		});

		return rdata({
			message: "ok",
			users,
		});
	}
);
