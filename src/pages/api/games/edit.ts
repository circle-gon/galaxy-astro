import { Game, editGameSchema } from "~/models/game";
import { rfu, rmsg } from "~/helper/res";
import { Channel } from "~/models/channel";
import { Comment } from "~/models/comment";
import { Favorite } from "~/models/favorite";
import { Rating } from "~/models/rating";
import { Update } from "~/models/update";
import { checkGameLink } from "~/helper/autoChecks";
import { env } from "~/helper/env";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import sharp from "sharp";
import { updateThumbs } from "~/helper/img";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	new: editGameSchema,
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: req.id });
		if (!game) return rmsg("Game not found", 400);

		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);

		if (req.new.name === "" && req.new.link === "") {
			const original = game.thumbTimestamp;
			game.thumbTimestamp = Date.now();
			await game.save();

			await Update.deleteMany({ game: game.id });
			await Rating.deleteMany({ game: game.id });
			await Comment.deleteMany({ game: game.id });
			await Favorite.deleteMany({ game: game.id });
			await updateThumbs(
				sharp("public/unknown-game.webp"),
				game.id,
				game.thumbTimestamp,
				original
			);

			await Game.deleteOne({ id: game.id });

			return rmsg("Game deleted.", 201);
		} else {
			if (req.new.name !== undefined) {
				game.name = req.new.name;
				await Channel.updateOne(
					{
						id: `game-${game.id}`,
					},
					{
						name: game.name,
					}
				);
			}
			if (req.new.description !== undefined) game.description = req.new.description;

			if (req.new.link !== undefined && req.new.link !== game.link) {
				const check = await checkGameLink(req.new.link);
				if (check.success) {
					req.new.link = check.finalUrl;
				} else {
					return rmsg(check.message, 400);
				}
				
				if (game.verified === true && game.link != req.new.link) {
					game.verified = false;
					if (env.STAFF_GAME_WEBHOOK !== undefined)
						await sendWebhook(env.STAFF_GAME_WEBHOOK, {
							content: `Updated game ${game.name} needs re-verification:\nhttps://galaxy.click/play/${game.id}\n<https://galaxy.click/admin>`,
						});
				}
				game.link = req.new.link;
			}

			await game.save();

			return rmsg("Updated!", 201);
		}
	}
);
