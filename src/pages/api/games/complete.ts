import { rdata, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { UserCompletedGame } from "~/models/usercompletedgame";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: req.game });
		if (game === null || game.verified !== true) {
			return rmsg("Game not found", 404);
		}

		let completed = await UserCompletedGame.findOne({
			game: game.id,
			user: user.id,
		});

		if (completed && completed.isOutdated) {
			await completed.deleteOne();
		} else if (completed) {
			// Already completed, delete it
			await completed.deleteOne();
			return rdata({
				message: "Game marked as not completed",
				completed: false,
			});
		}

		completed = new UserCompletedGame({
			game: game.id,
			user: user.id,
		});
		await completed.save();

		return rdata({
			message: "Game completed (congratulations!)",
			completed: true,
		});
	}
);
