---
layout: ~/layouts/Doc.astro
title: galaxy docs - save_list response
---

# `save_list`

The `save_list` response is requested by the [`save_list`](/docs/dev/actions/save_list) action and contains the requested list of save data.

## responding action

- [`save_list`](/docs/dev/actions/save_list)

## properties

- **`error`** *(boolean)*  
  Whether the action encountered an error.

- **`message`** *(string?)*  
  Present when `error` is true, tells the reason why the action encountered an error. Valid values are:
  + `"no_account"`: The player was logged out.
  + `"server_error"`: The game couldn't connect to Galaxy's servers.
  
- **`list`** *(object)*  
  A list of saves. Each entry in the object is an object containing save data, with the id being its save slot number. Will be an empty object (`{}`) when `error` is `true`.

  + **`list.*.label`** *(string)*  
	The label describing the save.

  + **`list.*.summary`** *(string)*  
	The machine-readable summary describing the save.

  + **`list.*.length`** *(number)*  
	The size of the save data, in bytes.

  + **`list.*.content`** *(string?)*  
	The actual save data. Can be empty.

<note warning>

In order to avoid unnecessary bandwidth, the save list respond omits save content if one exceeds 8,000 bytes in length. Use the [`load`](../actions/load) action when you need to actually load the save, and utilize the `summary` fields when saving data needed to generate summaries to be displayed to the player, as their content will always be available when you fetch the save list.

</note>

## examples

If a list was found:

```js
{
	type: "save_list",
	list: {
		10: {
			label: "1,000 points",
			summary: "{\"points\": 1000}",
			length: 31,
			content: "{\"points\": 1000, \"options\": {}}",
		}
	}
}
```

If a list wasn't found:

```js
{
	type: "save_list",
	error: true,
	message: "no_account",
	list: {},
}
```
