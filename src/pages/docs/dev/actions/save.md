---
layout: ~/layouts/Doc.astro
title: galaxy docs - save action
---


# `save`

The `save` action creates a cloud save and then puts it into a certain save slot. If a save is already present in that slot, the old save will be overridden. The save result can be determined through the returning [`saved`](/docs/dev/responses/saved) response.

## returning response

- [`saved`](/docs/dev/responses/saved)

## parameters

- **`slot`** *(number?)*  
  The save slot number. Must be an integer between 0 and 10, inclusive. If omitted, defaults to saving to slot number 0.

- **`label`** *(string?)*  
  The optional label of the save file.

- **`summary`** *(string?)*  
  A brief, machine-parseable summary of the save data. The purpose of this field is to be
  used by the game to generate save summaries without having to load the entire save, 
  which the `save_list` response can omit if the save data is deemed too large to be transmit within the save list. The content of it is not visible to the player nor there is an exposed way for the player to edit this field, thus it can be assumed that a save with an empty summary is manually saved by the player. Must be 2,048 bytes in length or fewer.

- **`data`** *(string)*  
  The actual save data. Must be 256,000 bytes in length or fewer.

## example

```js
window.top.postMessage({
	action: "save",
	slot: 0,
	label: "1,000 points",
	summary: "{\"points\": 1000}",
	data: "{\"points\": 1000, \"options\": {}}",
}, "https://galaxy.click")
```
