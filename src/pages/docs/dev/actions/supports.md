---
layout: ~/layouts/Doc.astro
title: galaxy docs - supports action
---


# `supports`

The `supports` action tells galaxy what parts of the API you interact with, and hides/shows certain elements of the player accordingly.

## parameters

- **`saving`** *(boolean)*  
  If your game auto-saves or allows the user to make/load game saves from within the UI.

<!-- save_manager should likely not be used at all.
the galaxy UI for cloud saving shouldn't be hidden even if
the game has save_manager, just so that -->
<!-- - **`save_manager`** *(boolean)*  
  If your game has a complete save manager integrated into it -- save deletion, export, import, labels, all save slots, etc. -->

- **`eval`** *(boolean)*  
  If your game will voluntarily allow `eval` calls from messages from galaxy. In the future, this may be used to enable auto-clickers, macros, userscripts, etc.

## example

```js
window.top.postMessage({
	action: "supports",
	saving: true,
	eval: true,
}, "https://galaxy.click");
```
