---
layout: ~/layouts/Doc.astro
title: galaxy docs - info action
---


# `info`

The `info` action re-retrieves the [`info`](/docs/dev/responses/info) response. This is useful if the initial info object is sent before your game loads.

## returning response

- [`info`](/docs/dev/responses/info)

## example

```js
window.top.postMessage({
	action: "info",
}, "https://galaxy.click");
```
